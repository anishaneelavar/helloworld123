﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld123
{
    class Program
    {
        static void Main(string[] args)
        {


            #region Dino Stuff

            Console.WriteLine("************** DINO STUFF BEGINS **************");
            Console.WriteLine("  ");

            //create a basic dino object.
            //var temp = new Dinosaur();

            //var tempDinoString = temp.ToString();

            //Console.WriteLine(tempDinoString);

            #endregion

            //Console.WriteLine("Enter your name to call the three functions");
            //string name = Console.ReadLine().ToString();

            //fun1();
            //fun2(name);
            //string a = fun3(name);
            //Console.WriteLine(a);

            string dname = "TRex";
            int dheight = 100;
            int dweight = 102;
            string dterrain = "CH";
            int did = 1;

            var dinoo = new Dinosaur(dname, dterrain, dheight, dweight, did);

            var tempDinoString2 = dinoo.ToString();



            //var dino3 = new Dinosaur();
            //dino3 = dino3.input();
            //dino3.display();
            //var tempDinoString3 = dino3.ToString();



            var CollectionOfDino = new List<Dinosaur>();

            //CollectionOfDino.Add(temp);
            //CollectionOfDino.Add(dinoo);
            CollectionOfDino = AddTenDinos(dinoo);






            //CollectionOfDino.Add(dino3);


            //diplay all dino data
            Console.WriteLine("*******--------*********UNSORTED***********---------*********");
            DisplayDinoCollection(CollectionOfDino);
            Console.WriteLine("--------------------------");





            Console.WriteLine("*******--------*********SORTED***********---------*********");
            var CollectionOfDino_Sorted = CollectionOfDino.OrderByDescending(x => x.DinoHeight).ToList();
            DisplayDinoCollection(CollectionOfDino_Sorted);
            Console.WriteLine("-------------------------");



            //ShowDinosBasedOnCountry(CollectionOfDino);


            //ShowUserDinosBasedOnCountry(CollectionOfDino);

            //ShowUniqueDinos(CollectionOfDino);

            DeleteDino(CollectionOfDino);

            Console.WriteLine("Press any key to exit.");


            Console.ReadKey();
        }

        private static void DeleteDino(List<Dinosaur> collectionOfDino)
        {
            var message = "";

            //display message 
            message = "Enter a id and we will delete that dino.";
            Console.WriteLine(message);
            var enteredDinoID = Console.ReadLine();
            var DinoIDNumber = Convert.ToInt32(enteredDinoID);

            //find the dino with the entered id. 
            var DinoFound = collectionOfDino.Select(x => x).Where(x => x.DinoId == DinoIDNumber).FirstOrDefault();

            if (DinoFound == null)
            {
                //display message 
                message = "Dino with ID - " + DinoIDNumber + "does not exist in our collection";
                Console.WriteLine(message);
            }
            else
            {
                collectionOfDino.Remove(DinoFound);
                message = "Dino with ID - " + DinoIDNumber + "has been deleted";
                Console.WriteLine(message);
            }

            DisplayDinoCollection(collectionOfDino);
        }

        private static void ShowUniqueDinos(List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("Enter dino name");
            string dinoname = Console.ReadLine().ToString();
            Console.WriteLine("Enter dino terrain");
            string dinoterrain = Console.ReadLine().ToString();
            Console.WriteLine("Enter dino height");
            int dinoheight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter dino weight");
            int dinoweight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter dino id");
            int dinoid = Convert.ToInt32(Console.ReadLine());
            var existingID = collectionOfDino.Select(x => x).Where(x => x.DinoId == dinoid).ToList();
            if(existingID.Count>=1)
            {
                Console.WriteLine("Id already exists");
               
                   var list = collectionOfDino.Select(x => x).OrderByDescending(x => x.DinoId).ToList();
                   var listlast = list[0].DinoId;
                   Console.WriteLine("Assigned Id : " + (listlast+1));
                var newdino = new Dinosaur(dinoname, dinoterrain, dinoheight, dinoweight, (listlast + 1));
                collectionOfDino.Add(newdino);
                DisplayDinoCollection(collectionOfDino);
            }
            else
            {
                var newdino = new Dinosaur(dinoname, dinoterrain, dinoheight, dinoweight, dinoid);
                collectionOfDino.Add(newdino);
                DisplayDinoCollection(collectionOfDino);
            }



        }

        private static void ShowUserDinosBasedOnCountry(List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("*******--------*********USER INPUT COUNTRY CODE***********---------*********");
                Console.WriteLine("Enter a country code - IN / AM / CH ");
                var code = (Console.ReadLine()).ToString();


                if (code != "AM" || code != "IN" || code != "CH")
                {
                    Console.WriteLine("Enter valid input");
                }


                var UserDinoList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == code).ToList();

                DisplayDinoCollection(UserDinoList);
            
        }

        private static void ShowDinosBasedOnCountry(List<Dinosaur> collectionOfDino)
        {
            
            var DinoIndiaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "IN").ToList();

            var DinoAmericaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "AM").ToList();

            var DinoChinaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "CH").ToList();


            Console.WriteLine("*******--------*********SORTED INDIA***********---------*********");
            DisplayDinoCollection(DinoIndiaList);


            Console.WriteLine("*******--------*********SORTED AMERICA***********---------*********");
            DisplayDinoCollection(DinoAmericaList);


            Console.WriteLine("*******--------*********SORTED CHINA***********---------*********");
            DisplayDinoCollection(DinoChinaList);
            

        }

        static void BasicTypeStuff()
        {

            Console.WriteLine("-------------------------string stuff--------------------");

            Console.WriteLine("Hello World!");



            #region basic int stuff

            //int a = 5;
            //int b = a + 2; //OK

            //bool test = true;

            //// Error. Operator '+' cannot be applied to operands of type 'int' and 'bool'.
            //int c = a + test;

            // Keep the console window open in debug mode.

            #endregion

            #region basic string stuff
            String string1 = "Exciting times ";
            String string2 = "lie ahead of us";

            String combineTheTwoStrings = string1 + string2;

            Console.WriteLine(combineTheTwoStrings);

            #endregion

            #region basic bool stuff. 
            Console.WriteLine("-------------------bool stuff----------------------------");

            //taking input
            Console.WriteLine("Enter a number please");
            //var input = Console.ReadLine().ToString();
            var input = "5";

            //converting the input (it will be in string format) to a int type

            int number = 0;
            try
            {
                number = Convert.ToInt32(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("Got some error - {0}", e.ToString());
                //assign a default number in case of error to resume code flow
                number = 10;
            }


            //we need a bool flag. 
            bool flag;

            if (number > 5)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            //lets display the value of the flag in the output.
            Console.WriteLine("The value of flag is {0}", flag);
            #endregion
        }
        static void fun1()
        {
            Console.WriteLine("Hello! I'm in function 1");
        }
        static void fun2(string name)
        {
            Console.WriteLine("Hello I'm in function 2" + "  " + name);

        }
        static string fun3(string name)
        {
            return "Hello I'm in function 3" + "  " + name;
        }

        private static void DisplayDinoCollection(List<Dinosaur> collectionOfDino)
        {
            //throw new NotImplementedException();

            //get total dinos.
            var totalDinos = collectionOfDino.Count;

            var message = "";

            //display total dinos for reference
            message = "Total Number of Dinos - " + totalDinos;
            Console.WriteLine(message);

            //loop through each dino.
            foreach (var dino in collectionOfDino)
            {
                //display dino details using the already existing display function
                dino.display();

                //put a simple line to indicate that a new dino will be displated in the next iteration
                message = "--------------------";
                Console.WriteLine(message);
            }
        }
        private static List<Dinosaur> AddTenDinos(Dinosaur s)
        {
            var dinoo2 = new Dinosaur("Allosaurus", "IN", 98, 645, 2);
            var dinoo3 = new Dinosaur("Apatosaurus", "IN", 76, 564, 3);
            var dinoo4 = new Dinosaur("Brachiosaurus", "IN", 97, 234, 4);
            var dinoo5 = new Dinosaur("Diplodocus", "AM", 67, 344, 5);
            var dinoo6 = new Dinosaur("Hypsilophodon", "AM", 99, 356, 6);
            var dinoo7 = new Dinosaur("Lesothosaurus", "AM", 78, 235, 7);
            var dinoo8 = new Dinosaur("Mamenchisaurus", "CH", 67, 563, 8);
            var dinoo9 = new Dinosaur("Notoceratops", "CH", 98, 254, 9);
            var dinoo10 = new Dinosaur("Seismosaurus", "CH", 68, 566, 10);

            
            var rCollectionOfDino = new List<Dinosaur>();

            rCollectionOfDino.Add(s);
            rCollectionOfDino.Add(dinoo2);
            rCollectionOfDino.Add(dinoo3);
            rCollectionOfDino.Add(dinoo4);
            rCollectionOfDino.Add(dinoo5);
            rCollectionOfDino.Add(dinoo6);
            rCollectionOfDino.Add(dinoo7);
            rCollectionOfDino.Add(dinoo8);
            rCollectionOfDino.Add(dinoo9);
            rCollectionOfDino.Add(dinoo10);

            return rCollectionOfDino;



        }
    }
}


