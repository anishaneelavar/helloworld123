﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld123
{
    //class Dinosaur holds basic information about various Dinosaurs
    class Dinosaur
        //ANISHA
    {
        public string DinoName { get; set; }

        public int DinoHeight { get; set; }

        public int DinoWeight { get; set; }

        public string DinoTerrain{get;set;}
        public int DinoId { get; set; }

        //TODO - Day1

        //default constructor

        //custom constructor with parameters

        //function do take input for Dino

        //function to display Dino

        //function to print all Dino Names

       public Dinosaur()
        {
            DinoName = "Some Dino Name";

            DinoTerrain = "CH";

            DinoHeight = 104;

            DinoWeight = 109;
            DinoId = 2;
        }

        public Dinosaur(string DinoName,string DinoTerrain, int DinoHeight, int DinoWeight, int DinoId)
        {
            this.DinoName = DinoName;
            this.DinoTerrain = DinoTerrain;
            this.DinoHeight = DinoHeight;
            this.DinoWeight = DinoWeight;
            this.DinoId = DinoId;

        }

        //public Dinosaur input()

        //{
        //    Console.WriteLine("-----------CUSTOM INPUT-------------");
        //    Console.WriteLine("Enter dino name");
        //    string dinoname = Console.ReadLine().ToString();
        //    Console.WriteLine("Enter dino terrain");
        //    string dinoterrain = Console.ReadLine().ToString();
        //    Console.WriteLine("Enter dino height");
        //    int dinoheight = Convert.ToInt32(Console.ReadLine());
        //    Console.WriteLine("Enter dino weight");
        //    int dinoweight = Convert.ToInt32(Console.ReadLine());
        //    Console.WriteLine("Enter dino id");
        //    int dinoid = Convert.ToInt32(Console.ReadLine());
        //    var toReturnDino = new Dinosaur(dinoname, dinoterrain, dinoheight, dinoweight,dinoid);

        //    return toReturnDino;
        //}
        public void display()
        {
            Console.WriteLine("------------------------");
            Console.WriteLine("DINO NAME : "+this.DinoName);
            Console.WriteLine("DINO TERRAIN : " + this.DinoTerrain);
            Console.WriteLine("DINO HEIGHT : " + this.DinoHeight);
            Console.WriteLine("DINO WEIGHT : " + this.DinoWeight);
            Console.WriteLine("DINO ID : " + this.DinoId);

        }
        //TODO - day2 - Collections LINQ and lambda 

        //add a unique id to each dino.

        //Collection of Dino Stuff

        //Add some dinos to this collection

        //make sure that no dino has more than one id.

        //after every addition show the collection

        //search a dino based on id

        //remove a dino. 

        //

    }
}
